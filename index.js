// This is our javascript code!

// Examples:

// print to the console
console.log("hello")
console.log(2 + 4)
var name = "Guy"
console.log("hello ", name)


// get an element that is on the page by its id
document.getElementById("button1").onclick = function(event) {
    console.log(event)
}

// get an element that is on the page by its classname
document.getElementsByClassName("button-class")[0].onclick = function(event) {
    alert("woaaahhh")
}


var myInput = document.getElementById("myInput");

document.getElementById("alertButton").onclick = function(event) {
    const myText = myInput.value;

    alert(myText);
}